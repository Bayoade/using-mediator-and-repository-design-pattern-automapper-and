﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagement.Core.Models;

namespace FleetManagement.Data
{
    public class DataMapperProfile : Profile
    {
        public DataMapperProfile()
        {
            CreateMap<UpdateCarCommand, Car>();

            CreateMap<CreateCarCommand, Car>();
        }
    }
}
