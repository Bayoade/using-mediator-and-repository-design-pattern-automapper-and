﻿using AutoMapper;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using FleetManagement.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FleetManagement.Data.Repository
{
    public class CarRepository : ICarRepository
    {
        private readonly FleetDbContext _dbContext;

        public CarRepository(FleetDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task CreateCarAsync(Car car)
        {
            _dbContext.Cars.Add(car);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteCarAsync(params Guid[] ids)
        {
            var cars = new List<Car>();

            foreach(var id in ids)
            {
                cars.Add(new Car { Id = id });
            }
            
            _dbContext.Cars.AttachRange(cars);

            _dbContext.Cars.RemoveRange(cars);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<Car>> GetAllCarAsync()
        {
            return await _dbContext.Cars.ToListAsync();
        }

        public Task<Car> GetCarByIdAsync(Guid id)
        {
            return _dbContext.Cars.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateCarAsync(Car car)
        {
            var carDb = await _dbContext.Cars.FirstOrDefaultAsync(x => x.Id == car.Id);

            Mapper.Map(car, carDb);

            _dbContext.Cars.Update(carDb);

            await _dbContext.SaveChangesAsync();
        }
    }
}
