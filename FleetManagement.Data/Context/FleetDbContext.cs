﻿using FleetManagement.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FleetManagement.Data.Context
{
    public class FleetDbContext : DbContext
    {
        public FleetDbContext(DbContextOptions<FleetDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");
            var carBuilder = builder.Entity<Car>().ToTable("Cars");
            carBuilder.HasKey(x => new { x.Id, x.UserId });
        }

        internal DbSet<Car> Cars { get; set; }
    }
}
