﻿using System;

namespace FleetManagement.Core.Models
{
    public class Car
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string YearOfManufacture { get; set; }

        public Guid UserId { get; set; }

        public string YearOfPurchase { get; set; }
    }
}
