﻿using Microsoft.Extensions.Configuration;

namespace FleetManagement.Core.Models
{
    public class AppSettings : IAppSettings
    {
        public AppSettings(IConfiguration configuration)
        {
            DbConnectionString = configuration.GetConnectionString("FleetManagement.Key");
            LoggingDbConnectionString = configuration.GetConnectionString("LoggingDBConnection");
        }

        public string DbConnectionString { get; }

        public string LoggingDbConnectionString { get; }
    }

    public interface IAppSettings
    {
        string DbConnectionString { get; }

        string LoggingDbConnectionString { get; }
    }
}
