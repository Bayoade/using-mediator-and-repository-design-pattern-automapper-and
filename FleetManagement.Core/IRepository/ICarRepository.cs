﻿using FleetManagement.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FleetManagement.Core.IRepository
{
    public interface ICarRepository
    {
        Task CreateCarAsync(Car car);

        Task DeleteCarAsync(params Guid[] ids);

        Task UpdateCarAsync(Car car);

        Task<Car> GetCarByIdAsync(Guid id);

        Task<IList<Car>> GetAllCarAsync();
    }
}
