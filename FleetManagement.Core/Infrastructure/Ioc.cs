﻿using Microsoft.Extensions.Configuration;

namespace FleetManagement.Core.Infrastructure
{
    public static class Ioc
    {
    }

    public static class IocContainer
    {
        public static IConfiguration Configuration { get; set; }
    }
}
