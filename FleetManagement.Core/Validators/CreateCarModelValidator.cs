﻿using FleetManagement.Core.Commands;
using FluentValidation;
using System;

namespace FleetManagement.Core.Validators
{
    public class CreateCarModelValidator : AbstractValidator<UpdateCarCommand>
    {
        public CreateCarModelValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(default(Guid))
                .NotNull().WithMessage("The car id is required");
        }
    }
}
