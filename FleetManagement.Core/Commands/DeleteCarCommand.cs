﻿using MediatR;
using System;
namespace FleetManagement.Core.Commands
{
    public class DeleteCarCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }
}
