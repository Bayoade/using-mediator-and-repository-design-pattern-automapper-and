﻿using MediatR;
using System;

namespace FleetManagement.Core.Commands
{
    public class UpdateCarCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string YearOfManufacture { get; set; }

        public Guid UserId { get; set; }

        public string YearOfPurchase { get; set; }
    }
}
