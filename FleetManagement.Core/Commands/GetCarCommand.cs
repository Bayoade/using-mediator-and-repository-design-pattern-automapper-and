﻿using FleetManagement.Core.Models;
using MediatR;
using System;

namespace FleetManagement.Core.Commands
{
    public class GetCarCommand : IRequest<CarDetailModel>
    {
        public Guid Id { get; set; }
    }
}
