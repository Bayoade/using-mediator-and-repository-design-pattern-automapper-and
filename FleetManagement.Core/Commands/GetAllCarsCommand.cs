﻿using FleetManagement.Core.Models;
using MediatR;
using System.Collections.Generic;

namespace FleetManagement.Core.Commands
{
    public class GetAllCarsCommand : IRequest<IList<CarDetailModel>>
    {
    }
}
