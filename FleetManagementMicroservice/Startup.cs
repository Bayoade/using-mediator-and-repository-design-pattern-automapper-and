﻿using Autofac;
using FleetManagement.Core.Infrastructure;
using FleetManagement.Handler;
using FleetManagementMicroservice.Bootstrap;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace FleetManagementMicroservice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            IocContainer.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextConfiguration();

            Assembly[] assemblies = new Assembly[]
            {
                typeof(CreateCarCommandHandler).GetTypeInfo().Assembly,
                typeof(DeleteCarCommandHandler).GetTypeInfo().Assembly,
                typeof(GetAllCarCommandHandler).GetTypeInfo().Assembly,
                typeof(GetCarCommandHandler).GetTypeInfo().Assembly,
                typeof(UpdateCarCommandHandler).GetTypeInfo().Assembly,
            };

            services.AddMediatR(assemblies);

            services.AddMapperProfileConfiguration();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerConfiguration();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.AddSwaggerToApplicationPipeline();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
