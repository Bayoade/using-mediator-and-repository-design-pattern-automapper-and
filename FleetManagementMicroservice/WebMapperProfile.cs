﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagementMicroservice.Models;

namespace FleetManagementMicroservice
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<SaveCarModel, CreateCarCommand>()
                .ForMember(x => x.Id, y => y.Ignore());

            CreateMap<SaveCarModel, UpdateCarCommand>()
                .ForMember(x => x.Id, y => y.Ignore());
        }
    }
}
