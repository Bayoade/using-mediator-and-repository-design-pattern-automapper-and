﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagementMicroservice.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FleetManagementMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CarsController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // GET api/values
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveCarModel model)
        {
            var createUserCommand = Mapper.Map<SaveCarModel, CreateCarCommand>(model);

            var newId = await _mediator.Send(createUserCommand);

            return Ok(newId);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _mediator.Send(new DeleteCarCommand { Id = id });

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update(Guid id, [FromBody] SaveCarModel model)
        {
            var updateUserCommand = Mapper.Map<SaveCarModel, UpdateCarCommand>(model);
            await _mediator.Send(updateUserCommand);

            return Ok();
        }
    }
}
