﻿using FleetManagement.Core.Infrastructure;
using FleetManagement.Core.Models;
using FleetManagement.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FleetManagementMicroservice.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<FleetDbContext>(conf =>
            {
                conf.UseSqlServer(new AppSettings(IocContainer.Configuration).DbConnectionString);
            }, ServiceLifetime.Scoped);
        }
    }
}
