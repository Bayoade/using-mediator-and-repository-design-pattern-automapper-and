﻿using Autofac;
using FleetManagement.Core.Behaviors;
using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using FleetManagement.Data.Repository;
using MediatR;

namespace FleetManagementMicroservice.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IMediator).GetType().Assembly)
            .AsImplementedInterfaces();

            builder.RegisterType<CarRepository>()
                .As<ICarRepository>()
                .InstancePerLifetimeScope();

            // Register all the Command classes (they implement IAsyncRequestHandler)
            // in assembly holding the Commands
            builder.RegisterAssemblyTypes(typeof(CreateCarCommand).GetType().Assembly).
                                       AsClosedTypesOf(typeof(IRequestHandler<,>));
            builder.RegisterAssemblyTypes(typeof(DeleteCarCommand).GetType().Assembly).
                                       AsClosedTypesOf(typeof(IRequestHandler<,>));
            builder.RegisterAssemblyTypes(typeof(GetAllCarsCommand).GetType().Assembly).
                                       AsClosedTypesOf(typeof(IRequestHandler<,>));
            builder.RegisterAssemblyTypes(typeof(UpdateCarCommand).GetType().Assembly).
                                       AsClosedTypesOf(typeof(IRequestHandler<,>));
            builder.RegisterAssemblyTypes(typeof(GetCarCommand).GetType().Assembly).
                                       AsClosedTypesOf(typeof(IRequestHandler<,>));
            builder.RegisterGeneric(typeof(LoggingBehavior<,>)).
                                                   As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).
                                                       As(typeof(IPipelineBehavior<,>));

            builder.RegisterType<AppSettings>()
                .As<IAppSettings>();
        }
    }
}
