﻿using AutoMapper;
using FleetManagement.Data;
using Microsoft.Extensions.DependencyInjection;

namespace FleetManagementMicroservice.Bootstrap
{
    public static class ConfigureMapperProfile
    {
        public static void AddMapperProfileConfiguration(this IServiceCollection services)
        {
            Mapper.Initialize(conf =>
            {
                conf.AddProfile<DataMapperProfile>();
                conf.AddProfile<WebMapperProfile>();
            });
        }
    }
}
