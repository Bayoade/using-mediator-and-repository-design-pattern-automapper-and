﻿using System;

namespace FleetManagementMicroservice.Models
{
    public class SaveCarModel
    {
        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string YearOfManufacture { get; set; }

        public Guid UserId { get; set; }

        public DateTime YearOfPurchase { get; set; }
    }
}
