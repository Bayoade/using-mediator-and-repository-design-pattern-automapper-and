﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Handler
{
    public class GetAllCarCommandHandler : IRequestHandler<GetAllCarsCommand, IList<CarDetailModel>>
    {
        private readonly ICarRepository _carRepository;

        public GetAllCarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public async Task<IList<CarDetailModel>> Handle(GetAllCarsCommand request, CancellationToken cancellationToken)
        {
            var cars = await _carRepository.GetAllCarAsync();

            return Mapper.Map<List<CarDetailModel>>(cars);
        }
    }
}
