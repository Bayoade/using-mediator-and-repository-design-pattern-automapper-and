﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Handler
{
    public class GetCarCommandHandler : IRequestHandler<GetCarCommand, CarDetailModel>
    {
        private readonly ICarRepository _carRepository;
        public GetCarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public async Task<CarDetailModel> Handle(GetCarCommand request, CancellationToken cancellationToken)
        {
            var car = await _carRepository.GetCarByIdAsync(request.Id);

            return Mapper.Map<CarDetailModel>(car);
        }
    }
}
