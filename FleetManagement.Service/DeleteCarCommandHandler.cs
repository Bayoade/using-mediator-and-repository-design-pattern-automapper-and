﻿using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Handler
{
    public class DeleteCarCommandHandler : IRequestHandler<DeleteCarCommand, bool>
    {
        private readonly ICarRepository _carRepository;
        public DeleteCarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public async Task<bool> Handle(DeleteCarCommand request, CancellationToken cancellationToken)
        {
            await _carRepository.DeleteCarAsync(request.Id);

            return await Task.FromResult(true);
        }
    }
}
