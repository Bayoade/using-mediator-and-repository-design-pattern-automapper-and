﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using FleetManagement.Core.Validators;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Handler
{
    public class UpdateCarCommandHandler : IRequestHandler<UpdateCarCommand, bool>
    {
        private readonly ICarRepository _carRepository;

        public UpdateCarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public async Task<bool> Handle(UpdateCarCommand request, CancellationToken cancellationToken)
        {
            var validator = new CreateCarModelValidator();

            var validationResult = validator.Validate(request);

            if (!validationResult.IsValid)
            {
                throw new Exception($"Invalid { request.GetType().Assembly } model {request}");
            }

            await _carRepository.UpdateCarAsync(Mapper.Map<Car>(request));

            return await Task.FromResult(true);
        }
    }
}
