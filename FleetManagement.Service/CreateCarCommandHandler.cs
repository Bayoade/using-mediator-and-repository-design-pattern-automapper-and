﻿using AutoMapper;
using FleetManagement.Core.Commands;
using FleetManagement.Core.IRepository;
using FleetManagement.Core.Models;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FleetManagement.Handler
{
    public class CreateCarCommandHandler : IRequestHandler<CreateCarCommand, Guid>
    {
        private readonly ICarRepository _carRepository;
        public CreateCarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public async Task<Guid> Handle(CreateCarCommand request, CancellationToken cancellationToken)
        {
            var car = Mapper.Map<CreateCarCommand, Car>(request);
            car.Id = Guid.NewGuid();

            await _carRepository.CreateCarAsync(car);

            return car.Id;
        }
    }
}
