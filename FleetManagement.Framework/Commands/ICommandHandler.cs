﻿using System.Threading.Tasks;

namespace FleetManagement.Framework.Commands
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        Task HandleAsync(T command);
    }
}
