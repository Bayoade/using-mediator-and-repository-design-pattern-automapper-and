﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FleetManagement.Framework.Commands
{
    public interface IAuthenticatedCommand : ICommand
    {
        Guid UserId { get; set; }
    }
}
