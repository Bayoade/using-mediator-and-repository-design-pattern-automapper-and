﻿using System;

namespace FleetManagement.Framework.Events
{
    public interface IAuthenticatedEvent : IEvent
    {
        Guid UserId { get; }
    }
}
