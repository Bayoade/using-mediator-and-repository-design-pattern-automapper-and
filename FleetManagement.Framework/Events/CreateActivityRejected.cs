﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FleetManagement.Framework.Events
{
    public class CreateActivityRejected : IRejectedEvent
    {
        public Guid Id { get; }
        public string Reason { get; }
        public string Code { get; }

        protected CreateActivityRejected() // For serialization
        {

        }

        public CreateActivityRejected(Guid id, string code, string reason)
        {
            this.Id = id;
            this.Code = code;
            this.Reason = reason;
        }
    }
}
